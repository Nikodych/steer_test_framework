package utils;

import lombok.experimental.UtilityClass;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.*;
import static org.apache.commons.lang3.text.WordUtils.capitalizeFully;

@UtilityClass
public class StringUtil {

    //  Upper case of the first letter of first word
    public static String capitalizeText(String text) {
        return capitalize((text.toLowerCase()));
    }

    //  Upper case of the first letter of every word
    public static String capitalizeFullyText(String text) {
        return capitalizeFully(text.toLowerCase());
    }

    public static String getLowerCaseTextWithoutSpaces(String text) {
        return text
                .replaceAll(SPACE, EMPTY)
                .trim()
                .toLowerCase();
    }

    public static String getEachWordCapitalized(String text) {
        return stream(text
                .replaceAll("_", SPACE)
                .trim()
                .split(SPACE))
                .map(word -> word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase())
                .collect(joining(" "));
    }
}
