package utils.runners;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import pageobjects.LoginPage;
import steps.EditAppointmentStep;
import steps.EditCustomerStep;
import utils.listeners.UiTestListener;

import static com.codeborne.selenide.Selenide.open;
import static java.time.Duration.ofSeconds;
import static utils.EnvProperties.*;

@Listeners(UiTestListener.class)
public abstract class BaseTestRunner {

    protected final EditCustomerStep editCustomerStep = new EditCustomerStep();
    protected final EditAppointmentStep editAppointmentStep = new EditAppointmentStep();
    protected LoginPage loginPage;

    @BeforeClass(alwaysRun = true, description = "Setup browser")
    public static void setupBrowser() {
        Configuration.browserSize = getBrowserSize();
        Configuration.timeout = ofSeconds(180).toMillis();
        Configuration.pageLoadTimeout = ofSeconds(300).toMillis();
        Configuration.browser = getBrowser();
        Configuration.screenshots = false;
        Configuration.savePageSource = false;
        Configuration.headless = isHeadless();

        var options = new ChromeOptions();
        options.addArguments("--no-sandbox",
                "--disable-site-isolation-trials",
                "--ignore-certificate-errors",
                "--disable-popup-blocking",
                "--disable-notifications",
                "--disable-browser-side-navigation",
                "--disable-gpu",
                "--dns-prefetch-disable",
                "--disable-impl-side-painting",
                "--disable-infobars",
                "--disable-dev-shm-usage");
        Configuration.browserCapabilities = options;

        open("about:blank");
    }

    @BeforeMethod(alwaysRun = true, description = "Opened login page")
    protected void openLoginPage() {
        open(getBaseURL());

        loginPage = new LoginPage();
    }

    @AfterMethod(description = "Closing web driver")
    protected void closeWebDriver() {
        WebDriverRunner.closeWebDriver();
    }
}
