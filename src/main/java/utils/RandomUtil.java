package utils;

import com.github.javafaker.Faker;
import lombok.experimental.UtilityClass;

import java.util.Random;

import static java.util.Locale.US;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static utils.StringUtil.getLowerCaseTextWithoutSpaces;

@UtilityClass
public class RandomUtil {

    private final Faker faker = new Faker(US);

    public static String getRandomString(int stringLength) {
        return randomAlphabetic(stringLength);
    }

    public static String addRandomSuffixToString(String string) {
        return string + getRandomString(5);
    }

    public static int getRandomNumber(int upperBound) {
        int randomNumber = new Random().nextInt(upperBound);
        randomNumber = randomNumber == 0 ? 1 : randomNumber;

        return randomNumber;
    }

    public static int getRandomNumber() {
        return getRandomNumber(9999999);
    }

    public static String getRandomFirstName() {
        return faker
                .name()
                .firstName();
    }

    public static String getRandomLastName() {
        return faker
                .name()
                .lastName();
    }

    public static String getRandomEmail() {
        return faker
                .internet()
                .emailAddress();
    }

    public static String getRandomEmailByLocalPart(String fullName) {
        return faker
                .internet()
                .emailAddress(getLowerCaseTextWithoutSpaces(fullName) + getRandomNumber());
    }

    public static String getRandomValidPassword() {
        return faker
                .internet()
                .password(8, 12);
    }

    public static String getRandomPhoneNumber() {
        return faker
                .phoneNumber()
                .phoneNumber();
    }

    public static String getRandomCompanyName() {
        return faker
                .company()
                .name();
    }
}
