package utils;

import lombok.experimental.UtilityClass;

import java.time.LocalDate;

import static com.google.common.base.Preconditions.checkArgument;
import static io.vavr.API.*;
import static java.lang.String.format;
import static utils.StringUtil.capitalizeText;

@UtilityClass
public class DateUtil {

    public static String getDayOfMonthSuffix(int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);

        if (n >= 11 && n <= 13) {
            return "th";
        }

        return Match(n % 10).of(
                Case($(1), () -> "st"),
                Case($(2), () -> "nd"),
                Case($(3), () -> "rd"),
                Case($(), () -> "th"));
    }

    public static String getFormattedDate(LocalDate date) {
        var dayOfMonth = date.getDayOfMonth();
        var dayOfWeek = capitalizeText(date.getDayOfWeek().name());
        var suffix = getDayOfMonthSuffix(dayOfMonth);
        var month = capitalizeText(date.getMonth().name());
        var year = date.getYear();

        return format("Choose %s, %s %s%s, %s", dayOfWeek, month, dayOfMonth, suffix, year);
    }
}
