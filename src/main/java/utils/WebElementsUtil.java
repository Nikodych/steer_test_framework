package utils;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.experimental.UtilityClass;
import org.awaitility.core.ConditionTimeoutException;
import org.openqa.selenium.WebElement;

import java.time.Duration;
import java.util.concurrent.Callable;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Configuration.timeout;
import static com.codeborne.selenide.Selenide.*;
import static java.lang.String.valueOf;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.function.Predicate.isEqual;
import static org.awaitility.Awaitility.await;
import static utils.AllureLogger.log;

@UtilityClass
public class WebElementsUtil {

    public static boolean isDisplayed(SelenideElement element, long timeoutMill) {
        try {
            return element
                    .shouldBe(visible, ofMillis(timeoutMill))
                    .isDisplayed();
        } catch (AssertionError error) {
            return false;
        }
    }

    public static boolean isDisplayed(SelenideElement element) {
        return isDisplayed(element, timeout);
    }

    @Step("[x] Waited for {element} to be displayed")
    public static boolean isDisplayed(SelenideElement element, Duration timeout) {
        try {
            return element
                    .shouldBe(visible, timeout)
                    .isDisplayed();
        } catch (AssertionError error) {
            return false;
        }
    }

    public static void waitTillVisible(SelenideElement element, Duration timeout) {
        isDisplayed(element, timeout);
    }

    @Step("[x] Waited for {element} to become not visible")
    public static void waitTillNotVisible(SelenideElement element, Duration timeout) {
        try {
            element.should(disappear, timeout);
        } catch (AssertionError ignored) {
        }
    }

    public static void slowInput(WebElement element, String value) {
        for (var singleChar : value.toCharArray()) {
            element.sendKeys(valueOf(singleChar));
        }
    }

    @Step("[x] Waited {seconds} second(s)")
    public static void loggedSleep(int seconds) {
        sleep(ofSeconds(seconds).toMillis());
    }

    @Step("[x] Waited for element to be displayed with refresh")
    public static boolean isElementWithRefreshDisplayed(String locator) {
        Callable<Boolean> elementIsDisplayed = () -> {
            if (isDisplayed($x(locator), ofSeconds(10))) {
                return true;
            }

            refresh();
            log("[x] Refreshed page");

            return false;
        };

        try {
            return await()
                    .atMost(5, MINUTES)
                    .pollInterval(20, SECONDS)
                    .ignoreExceptions()
                    .pollInSameThread()
                    .until(elementIsDisplayed, isEqual(true));
        } catch (ConditionTimeoutException e) {
            return false;
        }
    }
}
