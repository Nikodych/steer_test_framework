package models;

import enums.CustomerType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import static java.lang.String.format;

@Builder
@Getter
@Setter
public class Customer {
    private CustomerType customerType;
    private String firstName;
    private String lastName;
    private String companyName;
    private String insurance;
    private String mobileNumber;
    private String label;
    private String email;
    private boolean emailApprove;
    private boolean textMessageApprove;

    public String getFullName() {
        return format("%s %s", getFirstName(), getLastName());
    }
}
