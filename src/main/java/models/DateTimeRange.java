package models;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

@Builder
@Getter
public class DateTimeRange {
    private LocalDate start;
    private LocalDate end;
}
