package models;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Account {
    private String username;
    private String firstName;
    private String lastName;
    private String password;
}
