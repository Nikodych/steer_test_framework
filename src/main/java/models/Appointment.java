package models;

import enums.AppointmentStatus;
import enums.AppointmentType;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Appointment {
    private String title;
    private AppointmentType type;
    private Customer customer;
    private AppointmentStatus status;
    private DateTimeRange dateTimeRange;
    private String details;
}
