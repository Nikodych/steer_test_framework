package repo;

import models.Appointment;
import models.Customer;
import models.DateTimeRange;

import static enums.AppointmentStatus.BOOKED;
import static enums.AppointmentType.WAITING;
import static java.time.LocalDate.now;
import static utils.RandomUtil.addRandomSuffixToString;
import static utils.RandomUtil.getRandomString;

public class Appointments {

    public static Appointment getWaitingAndBookedAppointment(Customer customer) {
        var dateRange = DateTimeRange
                .builder()
                .start(now())
                .end(now())
                .build();

        return Appointment
                .builder()
                .title(addRandomSuffixToString("Test"))
                .type(WAITING)
                .customer(customer)
                .status(BOOKED)
                .dateTimeRange(dateRange)
                .details(getRandomString(100))
                .build();
    }
}
