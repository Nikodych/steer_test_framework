package repo;

import lombok.experimental.UtilityClass;
import models.Account;

import static utils.EnvProperties.getPassword;
import static utils.EnvProperties.getUsername;
import static utils.RandomUtil.getRandomEmail;
import static utils.RandomUtil.getRandomValidPassword;

@UtilityClass
public class Accounts {

    public static Account getDefaultAccount() {
        return Account
                .builder()
                .username(getUsername())
                .password(getPassword())
                .build();
    }

    public static Account getInvalidAccount() {
        return Account
                .builder()
                .username(getRandomEmail())
                .password(getRandomValidPassword())
                .build();
    }
}
