package repo;

import lombok.experimental.UtilityClass;
import models.Customer;

import static enums.CustomerType.CONSUMER;
import static utils.RandomUtil.*;

@UtilityClass
public class Customers {

    public static Customer getRandomConsumerCustomer() {
        var customer = Customer
                .builder()
                .customerType(CONSUMER)
                .firstName(getRandomFirstName())
                .lastName("Test-" + getRandomLastName())
                .companyName(getRandomCompanyName())
                .insurance(getRandomCompanyName())
                .mobileNumber(getRandomPhoneNumber())
                .label("Personal")
                .emailApprove(true)
                .textMessageApprove(true)
                .build();

        customer.setEmail(getRandomEmailByLocalPart(customer.getFullName()));

        return customer;
    }
}
