package steps;

import io.qameta.allure.Step;
import models.Appointment;
import pageobjects.CustomersDashboardPage;
import pageobjects.modals.EditAppointmentModal;

public class EditAppointmentStep {

    private final EditAppointmentModal editAppointmentModal = new EditAppointmentModal();

    @Step("Appointment created")
    public CustomersDashboardPage createAppointment(Appointment appointment) {
        editAppointmentModal
                .setTitle(appointment.getTitle())
                .setType(appointment.getType())
                .setCustomer(appointment.getCustomer())
                .setStatus(appointment.getStatus())
                .setDateRange(appointment.getDateTimeRange())
                .setDetails(appointment.getDetails())
                .create();

        return new CustomersDashboardPage();
    }
}
