package steps;

import io.qameta.allure.Step;
import models.Customer;
import pageobjects.CustomersDashboardPage;
import pageobjects.modals.EditCustomerModal;

public class EditCustomerStep {

    private final EditCustomerModal editCustomerModal = new EditCustomerModal();

    @Step("Customer created")
    public CustomersDashboardPage createCustomer(Customer customer) {
        editCustomerModal
                .setCustomerType(customer.getCustomerType())
                .setFirstName(customer.getFirstName())
                .setLastName(customer.getLastName())
                .setCompanyName(customer.getCompanyName())
                .setInsurance(customer.getInsurance())
                .setDefaultEmail(customer.getEmail())
                .setDefaultPhoneNumber(customer.getMobileNumber())
                .setLabel(customer.getLabel())
                .setEmailApprove(customer.isEmailApprove())
                .setTextMessageApprove(customer.isTextMessageApprove())
                .create();

        return new CustomersDashboardPage();
    }
}
