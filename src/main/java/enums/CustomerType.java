package enums;

import static utils.StringUtil.capitalizeText;

public enum CustomerType {
    CONSUMER, BUSINESS;

    public String getName() {
        return capitalizeText(name());
    }
}
