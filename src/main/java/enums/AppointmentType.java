package enums;

import static utils.StringUtil.getEachWordCapitalized;

public enum AppointmentType {
    WAITING, DROPPING_OFF, NONE;

    public String getName() {
        return getEachWordCapitalized(name());
    }
}
