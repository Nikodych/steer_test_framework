package enums;

import static utils.StringUtil.getEachWordCapitalized;

public enum AppointmentStatus {
    REQUEST_MADE, BOOKED, FINISHED, MISSED;

    public String getName() {
        return getEachWordCapitalized(name());
    }
}
