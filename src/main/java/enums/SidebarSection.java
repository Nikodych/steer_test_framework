package enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static org.apache.commons.lang3.StringUtils.EMPTY;

@AllArgsConstructor
public enum SidebarSection {

    CUSTOMERS("Dashboard"),
    VEHICLES("Dashboard"),
    SCHEDULER("Appointments"),
    CALENDAR("Appointments"),
    NOTIFICATIONS("Appointments"),
    TEXT_MESSAGES("Messages"),
    EMAILS("Messages"),
    ACTIVE("Campaigns"),
    DRAFT("Campaigns"),
    ARCHIVE("Campaigns"),
    REPUTATION(EMPTY);

    @Getter
    private final String sidebarMenu;
}
