package pageobjects;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static utils.AllureLogger.log;

public class LoginPage {

    public LoginPage setUsername(String username) {
        $("#username")
                .shouldBe(visible.because("Username input should be visible"))
                .setValue(username);
        log("Set username"); // instead of @Step, in which username will appear in report

        return this;
    }

    public LoginPage setPassword(String password) {
        $("#password")
                .shouldBe(visible.because("Password input should be visible"))
                .setValue(password);
        log("Set password"); // instead of @Step, in which password will appear in report

        return this;
    }

    @Step("Clicked on log in button")
    public CustomersDashboardPage logIn() {
        $x("//span[text()='Log In']/parent::button")
                .shouldBe(visible.because("Log in button should be visible"))
                .click();

        return new CustomersDashboardPage();
    }

    public String getWarningAlertDisplayed() {
        return $x("//div[contains(@class,'formAlert')]//span").getText();
    }
}
