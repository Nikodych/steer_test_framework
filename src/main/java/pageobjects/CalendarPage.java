package pageobjects;

import io.qameta.allure.Step;
import models.Appointment;

import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;
import static java.time.format.TextStyle.SHORT;
import static java.util.Locale.US;
import static org.apache.commons.lang3.StringUtils.SPACE;
import static utils.WebElementsUtil.isElementWithRefreshDisplayed;

public class CalendarPage extends BasePage {

    private final String appointmentSelector = "//div[@title='%s']//span[text()='%s']";

    public boolean isAppointmentDisplayed(Appointment appointment) {
        var customerFullName = appointment
                .getCustomer()
                .getFullName();

        return isElementWithRefreshDisplayed(format(appointmentSelector, appointment.getTitle(), customerFullName));
    }

    @Step("Opened appointment pop up")
    public CalendarPage openAppointmentPopUp(Appointment appointment) {
        var customerFullName = appointment
                .getCustomer()
                .getFullName();

        $x(format(appointmentSelector, appointment.getTitle(), customerFullName)).click();

        return this;
    }

    public boolean isAppointmentDataCorrect(Appointment appointment) {
        var appointmentDateRange = appointment.getDateTimeRange();
        var startDate = appointmentDateRange.getStart();
        var endDate = appointmentDateRange.getEnd();
        var popUpRootSelector = "//div[contains(@class,'eventDetails')]";
        var title = $x(popUpRootSelector + "/div[contains(@class,'header')]/div[contains(@class,'title')]").getText();
        var dateRange = $x(popUpRootSelector + "/div[contains(@class,'header')]//div[contains(@class,'tertiary')]").getText();
        var customer = $x(popUpRootSelector + "/div[contains(@class,'details')]//a[contains(@class,'customer')]").getText();
        var type = $x(popUpRootSelector + "/div[contains(@class,'details')]//div[contains(@class,'type')]").getText();

        return title.equals(appointment.getTitle()) && customer.equals(appointment.getCustomer().getFullName())
                && type.equals(appointment.getType().name()) && dateRange.contains(startDate.getMonth().getDisplayName(SHORT, US) + SPACE + startDate.getDayOfMonth())
                && dateRange.contains(endDate.getMonth().getDisplayName(SHORT, US) + SPACE + endDate.getDayOfMonth());
    }
}
