package pageobjects;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.ofSeconds;

public class CustomersDashboardPage extends BasePage {

    private final SelenideElement tableRowSelector = $x("//div[@role='row' and not(contains(@class,'headRow'))]");

    @Step("Searched customer by: {searchValue]")
    public CustomersDashboardPage searchCustomerBy(String searchValue) {
        $x("//div[contains(@class,'SearchField')]/input")
                .shouldBe(visible.because("Search field should be visible"), ofSeconds(5))
                .setValue(searchValue);

        return this;
    }

    @Step("Navigated to customer page")
    public CustomerPage navigateToCustomer() {
        //  will be implemented

        return new CustomerPage();
    }
}
