package pageobjects.modals;

import enums.CustomerType;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.checked;
import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;

public class EditCustomerModal extends BaseModal {

    private final String CHECKBOX_SELECTOR_TEMPLATE = "//span[text()='%s']/preceding-sibling::span[contains(@class,'Checkbox')]/input";

    public EditCustomerModal() {
        super($x(format(MODAL_ROOT_SELECTOR_TEMPLATE, "Customer")));
    }

    @Step("Set customer type: {customerType}")
    public EditCustomerModal setCustomerType(CustomerType customerType) {
        $x(format("//span[text()='%s']/ancestor::div[contains(@class,'checkboxRadioCard')]", customerType.getName())).click();

        return this;
    }

    @Step("Set first name: {firstName}")
    public EditCustomerModal setFirstName(String firstName) {
        setInputValue("firstname", firstName);

        return this;
    }

    @Step("Set last name: {lastName}")
    public EditCustomerModal setLastName(String lastName) {
        setInputValue("lastname", lastName);

        return this;
    }

    @Step("Set company name: {companyName}")
    public EditCustomerModal setCompanyName(String companyName) {
        setInputValue("companyname", companyName);

        return this;
    }

    @Step("Set insurance: {insurance}")
    public EditCustomerModal setInsurance(String insurance) {
        setInputValue("insurance", insurance);

        return this;
    }

    @Step("Set default email: {email}")
    public EditCustomerModal setDefaultEmail(String email) {
        setInputValue("emails.0.email", email);

        return this;
    }

    @Step("Set default phone number: {phoneNumber}")
    public EditCustomerModal setDefaultPhoneNumber(String phoneNumber) {
        setInputValue("phones.0.number", phoneNumber);

        return this;
    }

    @Step("Set phone label: {label}")
    public EditCustomerModal setLabel(String label) {
        setInputValue("phones.0.label", label);

        return this;
    }

    @Step("Set email approve to {approve}")
    public EditCustomerModal setEmailApprove(boolean approve) {
        var emailCheckbox = $x(format(CHECKBOX_SELECTOR_TEMPLATE, "Email"));

        if (emailCheckbox.has(not(checked)) && approve) {
            emailCheckbox.click();
        }

        return this;
    }

    @Step("Set text message approve to {approve}")
    public EditCustomerModal setTextMessageApprove(boolean approve) {
        var textMessageCheckbox = $x(format(CHECKBOX_SELECTOR_TEMPLATE, "Text message"));

        if (textMessageCheckbox.has(not(checked)) && approve) {
            textMessageCheckbox.click();
        }

        return this;
    }
}
