package pageobjects.modals;

import enums.AppointmentStatus;
import enums.AppointmentType;
import io.qameta.allure.Step;
import models.Customer;
import models.DateTimeRange;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;
import static utils.DateUtil.getFormattedDate;
import static utils.WebElementsUtil.loggedSleep;

public class EditAppointmentModal extends BaseModal {

    public EditAppointmentModal() {
        super($x(format(MODAL_ROOT_SELECTOR_TEMPLATE, "Appointment")));
    }

    @Step("Set appointment title: {value}")
    public EditAppointmentModal setTitle(String value) {
        setInputValue("title", value);

        return this;
    }

    @Step("Set first name: {type}")
    public EditAppointmentModal setType(AppointmentType type) {
        $x(format("//div[contains(@class,'switcher')]/div[text()='%s']", type.getName())).click();

        return this;
    }

    @Step("Set last name: {lastName}")
    public EditAppointmentModal setCustomer(Customer customer) {
        var fullName = customer.getFullName();
        var inputField = $x("//div[@name='customer']//input");
        inputField.setValue(customer.getLastName());

        //  workaround, sometimes the list with customers doesn't appear
        //TODO: a solution will be found
        loggedSleep(5);
        inputField.sendKeys(" \b");
        loggedSleep(5);

        var element = $$x("//div[contains(@class,'MenuList')]/div")
                .shouldBe(sizeGreaterThan(0).because("List of customers should not be empty"))
                .stream()
                .filter(option -> option.getText().equals(customer.getFullName()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No user found : " + fullName));

        element.click();

        return this;
    }

    @Step("Set status: {status}")
    public EditAppointmentModal setStatus(AppointmentStatus status) {
        var statusField = $x("//div[@name='status']");

        statusField.click();
        statusField.$x(format(".//*[text()='%s']", status.getName())).click();

        return this;
    }

    //  limited functionality for working with the calendar
    @Step("Set date range")
    public EditAppointmentModal setDateRange(DateTimeRange range) {
        var dateSelectorTemplate = "//div[@aria-label='%s']";
        var startDateFormatted = getFormattedDate(range.getStart());
        var endDateFormatted = getFormattedDate(range.getEnd());
        var datePickerIcon = $x("//div[@name='date']//div[contains(@class,'icon')]");

        datePickerIcon.click();
        $x(format(dateSelectorTemplate, startDateFormatted)).click();
        $x(format(dateSelectorTemplate, endDateFormatted)).click();
        datePickerIcon.click(); //  to close date picker

        return this;
    }

    @Step("Set details: {details}")
    public EditAppointmentModal setDetails(String details) {
        $x("//textarea[@name='details']").setValue(details);

        return this;
    }
}
