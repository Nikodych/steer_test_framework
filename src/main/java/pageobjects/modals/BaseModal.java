package pageobjects.modals;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.RequiredArgsConstructor;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;
import static java.time.Duration.ofSeconds;
import static utils.WebElementsUtil.isDisplayed;
import static utils.WebElementsUtil.waitTillVisible;

@RequiredArgsConstructor
public abstract class BaseModal {

    protected final static String MODAL_ROOT_SELECTOR_TEMPLATE = "//div[contains(@class,'text-center') and contains(text(),'%s')]/ancestor::div[@id='modal']";
    protected final SelenideElement modalRootElement;

    @Step("Created")
    public void create() {
        clickButton("Create");

        waitTillVisible(modalRootElement, ofSeconds(5));
    }

    protected boolean isModalDisplayed() {
        return isDisplayed(modalRootElement);
    }

    @Step("Closed modal")
    protected void closeModal() {
        modalRootElement
                .$x(".//button[contains(@class,'closeButton')]")
                .click();

        modalRootElement.shouldNotBe(visible.because("Modal should be closed"), ofSeconds(5));
    }

    protected void setInputValue(String inputName, String value) {
        $x(format("//input[contains(translate(@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '%s')]", inputName))
                .shouldBe(visible.because(inputName + " input should be visible"))
                .setValue(value);
    }

    @Step("Performed click on {buttonName} button")
    private void clickButton(String buttonName) {
        modalRootElement
                .$x(format(".//span[text()='%s']/parent::button", buttonName))
                .click();
    }
}
