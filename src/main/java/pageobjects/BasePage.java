package pageobjects;

import enums.SidebarSection;
import io.qameta.allure.Step;
import pageobjects.modals.EditAppointmentModal;
import pageobjects.modals.EditCustomerModal;

import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;
import static utils.StringUtil.getEachWordCapitalized;
import static utils.WebElementsUtil.isDisplayed;

public abstract class BasePage {

    private final String ACTION_ITEM_TEMPLATE = "//p[text()='%s']/ancestor::li";
    private final String headerAddButtonSelector = "//div[contains(@class,'HeaderAddButton')]";

    public boolean isUserLoggedIn(String username) {
        var profileNameSelector = format("//span[contains(@class,'ProfileControl__name')][text()='%s']", username);

        return isDisplayed($x(profileNameSelector));
    }

    @Step("Started adding new customer")
    public EditCustomerModal startAddingNewCustomer() {
        $x(headerAddButtonSelector).click();
        $x(format(ACTION_ITEM_TEMPLATE, "New Customer")).click();

        return new EditCustomerModal();
    }

    @Step("Started adding new appointment")
    public EditAppointmentModal startAddingNewAppointment() {
        $x(headerAddButtonSelector).click();
        $x(format(ACTION_ITEM_TEMPLATE, "New Appointment")).click();

        return new EditAppointmentModal();
    }

    @Step("Navigated to {section.name}")
    public void navigateTo(SidebarSection section) {
        var sectionName = getEachWordCapitalized(section.name());

        openSideBarMenuBy(section.getSidebarMenu());
        $x(format("//div[@id='sidebar-navigation']//div[text()='%s']", sectionName)).click();
    }

    @Step("Opened {name} sidebar menu")
    private void openSideBarMenuBy(String name) {
        var sideBarMenuSelector = format("//div[@id='sidebar-navigation']//div[text()='%s']/ancestor::div[contains(@class,'pointer')]", name);

        if (!$x(sideBarMenuSelector + "[contains(@class,'linkWrapperOpened')]").isDisplayed()) {
            $x(sideBarMenuSelector).click();
        }
    }
}
