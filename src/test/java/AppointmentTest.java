import io.qameta.allure.Description;
import org.testng.annotations.Test;
import pageobjects.CalendarPage;
import utils.runners.BaseTestRunner;

import static enums.SidebarSection.CALENDAR;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static repo.Accounts.getDefaultAccount;
import static repo.Appointments.getWaitingAndBookedAppointment;
import static repo.Customers.getRandomConsumerCustomer;

public class AppointmentTest extends BaseTestRunner {

    @Test
    @Description("Verification of adding new appointment")
    public void addNewAppointment() {
        var defaultAccount = getDefaultAccount();
        var newCustomer = getRandomConsumerCustomer();
        var appointment = getWaitingAndBookedAppointment(newCustomer);

        var dashboardPage = loginPage
                .setUsername(defaultAccount.getUsername())
                .setPassword(defaultAccount.getPassword())
                .logIn();

        dashboardPage.startAddingNewCustomer();
        editCustomerStep.createCustomer(newCustomer);

        dashboardPage.startAddingNewAppointment();
        editAppointmentStep.createAppointment(appointment);

        dashboardPage.navigateTo(CALENDAR);
        var calendarPage = new CalendarPage();

        assertThat(calendarPage.isAppointmentDisplayed(appointment))
                .withFailMessage("Appointment should be displayed")
                .isTrue();

        calendarPage.openAppointmentPopUp(appointment);

        assertThat(calendarPage.isAppointmentDataCorrect(appointment))
                .withFailMessage("Appointment data should match")
                .isTrue();
    }
}
