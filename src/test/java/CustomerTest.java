import io.qameta.allure.Description;
import utils.runners.BaseTestRunner;

import static enums.SidebarSection.CUSTOMERS;
import static repo.Accounts.getDefaultAccount;
import static repo.Customers.getRandomConsumerCustomer;

public class CustomerTest extends BaseTestRunner {

    //TODO: the app is currently not working correctly, doesn't show any data, can't fetch data from server
//    @Test
    @Description("Verification of adding new customer")
    public void addNewCustomer() {
        var defaultAccount = getDefaultAccount();
        var newCustomer = getRandomConsumerCustomer();

        var dashboardPage = loginPage
                .setUsername(defaultAccount.getUsername())
                .setPassword(defaultAccount.getPassword())
                .logIn();

        dashboardPage.startAddingNewCustomer();

        editCustomerStep.createCustomer(newCustomer);

        dashboardPage.navigateTo(CUSTOMERS);

        //  next steps and asserts
    }
}
