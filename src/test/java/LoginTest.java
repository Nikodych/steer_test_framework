import io.qameta.allure.Description;
import org.testng.annotations.Test;
import utils.runners.BaseTestRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static repo.Accounts.getDefaultAccount;
import static repo.Accounts.getInvalidAccount;
import static utils.AllureLogger.log;

public class LoginTest extends BaseTestRunner {

    @Test
    @Description("Verification of successful login to the site")
    public void successfulLogin() {
        var defaultAccount = getDefaultAccount();

        var dashboardPage = loginPage
                .setUsername(defaultAccount.getUsername())
                .setPassword(defaultAccount.getPassword())
                .logIn();

        assertThat(dashboardPage.isUserLoggedIn(defaultAccount.getUsername()))
                .withFailMessage("User should be logged in")
                .isTrue();

        log("User successfully logged in");
    }

    @Test
    @Description("Verification of an attempt to log in with incorrect credentials and get an alert")
    public void unsuccessfulLogin() {
        var invalidAccount = getInvalidAccount();

        loginPage
                .setUsername(invalidAccount.getUsername())
                .setPassword(invalidAccount.getPassword())
                .logIn();

        assertThat(loginPage.getWarningAlertDisplayed())
                .withFailMessage("User should get alert message")
                .isEqualTo("Incorrect email address or password! Please try again.");

        log("User failed to log in and received an alert");
    }
}
